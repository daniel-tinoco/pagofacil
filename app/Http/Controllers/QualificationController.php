<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Qualification;

class QualificationController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){

        $this->validate(
            $request,
            [
                'id_t_materias' => 'required|exists:t_materias',
                'id_t_usuarios' => 'required|exists:t_alumnos',
                'calificacion' => 'required',
            ],
            Qualification::$messages
        );

        $result = Qualification::create($request->all());

        if($result){
            return response()->json(array(
                'success' => 'ok',
                'msg' => 'calificacion registrada'
            ));
        }else{
            return response()->json(array(
                'success' => 'error',
                'msg' => 'Hubo un error al crear el registro'
            ));
        }

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id){
        $this->validate(
            $request,
            [
                'calificacion' => 'required',
            ],
            Qualification::$messages
        );

        $qualification = Qualification::find($id);

        if(!is_null($qualification)) {

            $qualification->calificacion = $request->get('calificacion');

            if($qualification->save()){
                return response()->json(array(
                    'success' => 'ok',
                    'msg' => 'calificacion actualizada'
                ));
            }else{
                return response()->json(array(
                    'success' => 'error',
                    'msg' => 'Hubo un error al actualizar el registro'
                ));
            }
        }else{
            return response()->json(array(
                'success' => 'error',
                'msg' => 'No se encontraron resultados'
            ));
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){

        $qualification = Qualification::find($id);

        if(!is_null($qualification)) {
            if ($qualification->delete()) {
                return response()->json(array(
                    'success' => 'ok',
                    'msg' => 'calificacion eliminada'
                ));
            } else {
                return response()->json(array(
                    'success' => 'error',
                    'msg' => 'Hubo un error al eliminar el registro'
                ));
            }
        }else{
            return response()->json(array(
                'success' => 'error',
                'msg' => 'No se encontraron resultados'
            ));
        }
    }

}

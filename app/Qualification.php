<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $table = "t_calificaciones";
    protected $primaryKey = "id_t_calificaciones";
    public $timestamps = false;

    protected $fillable = [
        'id_t_materias',
        'id_t_usuarios',
        'calificacion',
        'fecha_registro'
    ];

    public static $messages = [
        'id_t_materias.required' => 'El id de la materia es obligatorio',
        'id_t_materias.exists' => 'El id de la materia no existe :(',
        'id_t_usuarios.required' => 'El id del alumno es obligatorio',
        'id_t_usuarios.exists' => 'El id del alumno no existe :(',
        'calificacion.required' => 'La calificacion es obligatorio',
    ];

}
